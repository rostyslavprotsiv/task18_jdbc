package com.rostyslavprotsiv.view.interfaces;

@FunctionalInterface
public interface Printable {
    void print();
}
