package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.DAO.annotation.*;
import com.rostyslavprotsiv.model.DAO.annotation.enums.CascadeType;

import java.util.Objects;

@Table(name = "redirected_package")
public class RedirectedPackage {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @OneToOne(type = CascadeType.REFRESH)
    @Column(name = "sending_package_id")
    private int sendingPackageId;
    @OneToMany(type = CascadeType.REFRESH)
    @Column(name = "to_client_id")
    private int toClientId;
    @OneToMany(type = CascadeType.REMOVE)
    @Column(name = "to_point_id")
    private int toPointId;

    public RedirectedPackage() {
    }

    public RedirectedPackage(int id, int sendingPackageId, int toClientId,
                             int toPointId) {
        this.id = id;
        this.sendingPackageId = sendingPackageId;
        this.toClientId = toClientId;
        this.toPointId = toPointId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSendingPackageId() {
        return sendingPackageId;
    }

    public void setSendingPackageId(int sendingPackageId) {
        this.sendingPackageId = sendingPackageId;
    }

    public int getToClientId() {
        return toClientId;
    }

    public void setToClientId(int toClientId) {
        this.toClientId = toClientId;
    }

    public int getToPointId() {
        return toPointId;
    }

    public void setToPointId(int toPointId) {
        this.toPointId = toPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RedirectedPackage that = (RedirectedPackage) o;
        return id == that.id &&
                sendingPackageId == that.sendingPackageId &&
                toClientId == that.toClientId &&
                toPointId == that.toPointId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sendingPackageId, toClientId, toPointId);
    }

    @Override
    public String toString() {
        return "RedirectedPackage{" +
                "id=" + id +
                ", sendingPackageId=" + sendingPackageId +
                ", toClientId=" + toClientId +
                ", toPointId=" + toPointId +
                '}';
    }
}
