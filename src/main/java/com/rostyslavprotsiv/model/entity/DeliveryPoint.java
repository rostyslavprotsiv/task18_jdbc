package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.DAO.annotation.Column;
import com.rostyslavprotsiv.model.DAO.annotation.PrimaryKey;
import com.rostyslavprotsiv.model.DAO.annotation.Table;

import java.util.Objects;

@Table(name = "delivery_point")
public class DeliveryPoint {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "address", length = 45)
    private String address;
    @Column(name = "city", length = 45)
    private String city;
    @Column(name = "point_id", length = 45)
    private String pointId;

    public DeliveryPoint(int id, String address, String city, String pointId) {
        this.id = id;
        this.address = address;
        this.city = city;
        this.pointId = pointId;
    }

    public DeliveryPoint() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPointId() {
        return pointId;
    }

    public void setPointId(String pointId) {
        this.pointId = pointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliveryPoint that = (DeliveryPoint) o;
        return id == that.id &&
                Objects.equals(address, that.address) &&
                Objects.equals(city, that.city) &&
                Objects.equals(pointId, that.pointId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, address, city, pointId);
    }

    @Override
    public String toString() {
        return "DeliveryPoint{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", pointId='" + pointId + '\'' +
                '}';
    }
}
