package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.DAO.annotation.*;
import com.rostyslavprotsiv.model.DAO.annotation.enums.CascadeType;

import java.util.Objects;

@Table(name = "sending_package")
public class SendingPackage {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "sending_id")
    private int sendingId;
    @OneToMany(type = CascadeType.ALL)
    @Column(name = "from_client_id")
    private int fromClientId;
    @OneToMany(type = CascadeType.DETACH)
    @Column(name = "from_point_id")
    private int fromPointId;
    @OneToOne(type = CascadeType.ALL)
    @Column(name = "package_id")
    private int packageId;

    public SendingPackage() {
    }

    public SendingPackage(int id, int sendingId, int fromClientId,
                          int fromPointId, int packageId) {
        this.id = id;
        this.sendingId = sendingId;
        this.fromClientId = fromClientId;
        this.fromPointId = fromPointId;
        this.packageId = packageId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSendingId() {
        return sendingId;
    }

    public void setSendingId(int sendingId) {
        this.sendingId = sendingId;
    }

    public int getFromClientId() {
        return fromClientId;
    }

    public void setFromClientId(int fromClientId) {
        this.fromClientId = fromClientId;
    }

    public int getFromPointId() {
        return fromPointId;
    }

    public void setFromPointId(int fromPointId) {
        this.fromPointId = fromPointId;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SendingPackage that = (SendingPackage) o;
        return id == that.id &&
                sendingId == that.sendingId &&
                fromClientId == that.fromClientId &&
                fromPointId == that.fromPointId &&
                packageId == that.packageId;
    }

    @Override
    public int hashCode() {
        return Objects
                .hash(id, sendingId, fromClientId, fromPointId, packageId);
    }

    @Override
    public String toString() {
        return "SendingPackage{" +
                "id=" + id +
                ", sendingId=" + sendingId +
                ", fromClientId=" + fromClientId +
                ", fromPointId=" + fromPointId +
                ", packageId=" + packageId +
                '}';
    }
}
