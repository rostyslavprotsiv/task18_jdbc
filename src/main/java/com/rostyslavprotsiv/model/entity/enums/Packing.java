package com.rostyslavprotsiv.model.entity.enums;

public enum Packing {
    FULL, WITHOUT, POLYETHYLENE
}
