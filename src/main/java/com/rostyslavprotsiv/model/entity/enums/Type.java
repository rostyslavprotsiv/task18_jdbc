package com.rostyslavprotsiv.model.entity.enums;

public enum Type {
    KITCHEN, WEAPON, TOY, FISHING
}
