package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.DAO.annotation.Column;
import com.rostyslavprotsiv.model.DAO.annotation.PrimaryKey;
import com.rostyslavprotsiv.model.DAO.annotation.Table;

import java.util.Objects;

@Table(name = "client")
public class Client {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "name", length = 45)
    private String name;
    @Column(name = "surname", length = 45)
    private String surname;
    @Column(name = "middle_name", length = 45)
    private String middleName;
    @Column(name = "package_count")
    private int packageCount;
    @Column(name = "all_name", length = 45)
    private String allName;

    public Client() {
    }

    public Client(int id, String name, String surname, String middleName,
                  int packageCount, String allName) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.middleName = middleName;
        this.packageCount = packageCount;
        this.allName = allName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getPackageCount() {
        return packageCount;
    }

    public void setPackageCount(int packageCount) {
        this.packageCount = packageCount;
    }

    public String getAllName() {
        return allName;
    }

    public void setAllName(String allName) {
        this.allName = allName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id &&
                packageCount == client.packageCount &&
                Objects.equals(name, client.name) &&
                Objects.equals(surname, client.surname) &&
                Objects.equals(middleName, client.middleName) &&
                Objects.equals(allName, client.allName);
    }

    @Override
    public int hashCode() {
        return Objects
                .hash(id, name, surname, middleName, packageCount, allName);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", middleName='" + middleName + '\'' +
                ", packageCount=" + packageCount +
                ", allName='" + allName + '\'' +
                '}';
    }
}
