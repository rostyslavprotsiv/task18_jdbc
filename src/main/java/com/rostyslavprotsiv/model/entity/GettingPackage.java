package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.DAO.annotation.*;
import com.rostyslavprotsiv.model.DAO.annotation.enums.CascadeType;

import java.math.BigDecimal;
import java.util.Objects;

@Table(name = "getting_package")
public class GettingPackage {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "got")
    private boolean isGot;
    @Column(name = "price")
    private BigDecimal price;
    @OneToOne(type = CascadeType.PERSIST)
    @Column(name = "sending_package_id")
    private int sendingPackageId;
    @OneToMany(type = CascadeType.REFRESH)
    @Column(name = "to_client_id")
    private int toClientId;
    @OneToMany(type = CascadeType.REMOVE)
    @Column(name = "to_point_id")
    private int toPointId;

    public GettingPackage() {
    }

    public GettingPackage(int id, boolean isGot, BigDecimal price,
                          int sendingPackageId, int toClientId, int toPointId) {
        this.id = id;
        this.isGot = isGot;
        this.price = price;
        this.sendingPackageId = sendingPackageId;
        this.toClientId = toClientId;
        this.toPointId = toPointId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isGot() {
        return isGot;
    }

    public void setGot(boolean got) {
        isGot = got;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getSendingPackageId() {
        return sendingPackageId;
    }

    public void setSendingPackageId(int sendingPackageId) {
        this.sendingPackageId = sendingPackageId;
    }

    public int getToClientId() {
        return toClientId;
    }

    public void setToClientId(int toClientId) {
        this.toClientId = toClientId;
    }

    public int getToPointId() {
        return toPointId;
    }

    public void setToPointId(int toPointId) {
        this.toPointId = toPointId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GettingPackage that = (GettingPackage) o;
        return id == that.id &&
                isGot == that.isGot &&
                sendingPackageId == that.sendingPackageId &&
                toClientId == that.toClientId &&
                toPointId == that.toPointId &&
                Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects
                .hash(id, isGot, price, sendingPackageId, toClientId,
                        toPointId);
    }

    @Override
    public String toString() {
        return "GettingPackage{" +
                "id=" + id +
                ", isGot=" + isGot +
                ", price=" + price +
                ", sendingPackageId=" + sendingPackageId +
                ", toClientId=" + toClientId +
                ", toPointId=" + toPointId +
                '}';
    }
}
