package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.DAO.annotation.Column;
import com.rostyslavprotsiv.model.DAO.annotation.PrimaryKey;
import com.rostyslavprotsiv.model.DAO.annotation.Table;
import com.rostyslavprotsiv.model.entity.enums.Packing;
import com.rostyslavprotsiv.model.entity.enums.Type;

import java.math.BigDecimal;
import java.util.Objects;

@Table(name = "package")
public class Package {
    @PrimaryKey
    @Column(name = "id")
    private int id;
    @Column(name = "type")
    private Type type;
    @Column(name = "size")
    private double size;
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "weight")
    private double weight;
    @Column(name = "packaging")
    private Packing packing;

    public Package(int id, Type type, double size, BigDecimal price,
                   double weight, Packing packing) {
        this.id = id;
        this.type = type;
        this.size = size;
        this.price = price;
        this.weight = weight;
        this.packing = packing;
    }

    public Package() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Packing getPacking() {
        return packing;
    }

    public void setPacking(Packing packing) {
        this.packing = packing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Package aPackage = (Package) o;
        return id == aPackage.id &&
                Double.compare(aPackage.size, size) == 0 &&
                Double.compare(aPackage.weight, weight) == 0 &&
                type == aPackage.type &&
                Objects.equals(price, aPackage.price) &&
                packing == aPackage.packing;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, size, price, weight, packing);
    }

    @Override
    public String toString() {
        return "Package{" +
                "id=" + id +
                ", type=" + type +
                ", size=" + size +
                ", price=" + price +
                ", weight=" + weight +
                ", packing=" + packing +
                '}';
    }
}
