package com.rostyslavprotsiv.model.transformer;

import com.rostyslavprotsiv.model.DAO.annotation.Column;
import com.rostyslavprotsiv.model.DAO.annotation.Table;
import com.rostyslavprotsiv.model.entity.enums.Packing;
import com.rostyslavprotsiv.model.entity.enums.Type;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {
    private final Class<T> clazz;

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToEntity(ResultSet rs)
            throws SQLException {
        Object entity = null;
        try {
            entity = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(Table.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Column.class)) {
                        Column column = field.getAnnotation(Column.class);
                        String name = column.name();
                        field.setAccessible(true);
                        Class fieldType = field.getType();
                        if (fieldType == String.class) {
                            field.set(entity, rs.getString(name));
                        } else if (fieldType == Type.class) {
                            field.set(entity, Type
                                    .valueOf(rs.getString(name).toUpperCase()));
                        } else if (fieldType == Packing.class) {
                            field.set(entity, Packing
                                    .valueOf(rs.getString(name).toUpperCase()));
                        } else if (fieldType == int.class) {
                            field.set(entity, rs.getInt(name));
                        } else if (fieldType == double.class) {
                            field.set(entity, rs.getDouble(name));
                        } else if (fieldType == BigDecimal.class) {
                            field.set(entity, rs.getBigDecimal(name));
                        } else if (fieldType == boolean.class) {
                            field.set(entity, rs.getBoolean(name));
                        }
                    }
                }
            }
        } catch (InstantiationException | IllegalAccessException
                | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return entity;
    }
}
