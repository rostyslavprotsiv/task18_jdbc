package com.rostyslavprotsiv.model.connectionmanager;

import com.rostyslavprotsiv.model.action.resourcemanager.ResourceManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private static Connection connection = null;

    private ConnectionManager() {
    }

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(ResourceManager.
                                INSTANCE.getString("url"),
                        ResourceManager.INSTANCE.getString("user"),
                        ResourceManager.INSTANCE.getString("password"));
            } catch (SQLException e) {
                System.out.println("SQLException: " + e.getMessage());
                System.out.println("SQLState: " + e.getSQLState());
                System.out.println("VendorError: " + e.getErrorCode());
            }
        }
        return connection;
    }
}
