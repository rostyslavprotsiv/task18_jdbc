package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.DeliveryPoint;

import java.sql.SQLException;
import java.util.List;

public interface IDeliveryPointDAO extends IGeneralDAO<DeliveryPoint, Integer> {
    List<String> getAddressByCity(String city) throws SQLException;
}
