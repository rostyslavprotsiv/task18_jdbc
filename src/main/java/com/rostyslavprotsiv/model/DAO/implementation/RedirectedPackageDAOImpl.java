package com.rostyslavprotsiv.model.DAO.implementation;

import com.rostyslavprotsiv.model.DAO.IRedirectedPackageDAO;
import com.rostyslavprotsiv.model.connectionmanager.ConnectionManager;
import com.rostyslavprotsiv.model.entity.RedirectedPackage;
import com.rostyslavprotsiv.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class RedirectedPackageDAOImpl implements IRedirectedPackageDAO {
    private static final String FIND_ALL = "SELECT * FROM redirected_package";
    private static final String DELETE = "DELETE FROM redirected_package " +
            "WHERE id=?";
    private static final String CREATE = "INSERT redirected_package (id, " +
            "sending_package_id, to_client_id, to_point_id) VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE redirected_package SET " +
            "sending_package=?, to_client_id=?, to_point_id=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM " +
            "redirected_package WHERE id=?";
    private static final String CREATE_WITHOUT_ID = "INSERT " +
            "redirected_package (sending_package_id, to_client_id, to_point_id) " +
            "VALUES (?, ?, ?)";

    @Override
    public List<RedirectedPackage> findAll() throws SQLException {
        List<RedirectedPackage> redirectedPackages = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    redirectedPackages.add((RedirectedPackage)
                            new Transformer(RedirectedPackage.class)
                                    .fromResultSetToEntity(resultSet));
                }
            }
        }
        return redirectedPackages;
    }

    @Override
    public RedirectedPackage findById(Integer id) throws SQLException {
        RedirectedPackage entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (RedirectedPackage)
                            new Transformer(RedirectedPackage.class)
                                    .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(RedirectedPackage entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setInt(2, entity.getSendingPackageId());
            ps.setInt(3, entity.getToClientId());
            ps.setInt(4, entity.getToPointId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(RedirectedPackage entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setInt(1, entity.getSendingPackageId());
            ps.setInt(2, entity.getToClientId());
            ps.setInt(3, entity.getToPointId());
            ps.setInt(4, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public int createWithoutID(RedirectedPackage entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE_WITHOUT_ID)) {
            ps.setInt(1, entity.getSendingPackageId());
            ps.setInt(2, entity.getToClientId());
            ps.setInt(3, entity.getToPointId());
            return ps.executeUpdate();
        }
    }

}
