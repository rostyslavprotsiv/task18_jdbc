package com.rostyslavprotsiv.model.DAO.implementation;

import com.rostyslavprotsiv.model.DAO.IPackageDAO;
import com.rostyslavprotsiv.model.connectionmanager.ConnectionManager;
import com.rostyslavprotsiv.model.entity.Package;
import com.rostyslavprotsiv.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class PackageDAOImpl implements IPackageDAO {
    private static final String FIND_ALL = "SELECT * FROM package";
    private static final String DELETE = "DELETE FROM package WHERE id=?";
    private static final String CREATE = "INSERT package (id, type," +
            "size, price, weight packing) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE package SET type=?, " +
            "size=?, price=?, weight=?, packing=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM package WHERE id=?";


    @Override
    public List<Package> findAll() throws SQLException {
        List<Package> packages = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    packages.add((Package) new Transformer(Package.class)
                            .fromResultSetToEntity(resultSet));
                }
            }
        }
        return packages;
    }

    @Override
    public Package findById(Integer id) throws SQLException {
        Package entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (Package) new Transformer(Package.class)
                            .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Package entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getType().name());
            ps.setDouble(3, entity.getSize());
            ps.setBigDecimal(4, entity.getPrice());
            ps.setDouble(5, entity.getWeight());
            ps.setString(6, entity.getPacking().name());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Package entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getType().name());
            ps.setDouble(2, entity.getSize());
            ps.setBigDecimal(3, entity.getPrice());
            ps.setDouble(4, entity.getWeight());
            ps.setString(5, entity.getPacking().name());
            ps.setInt(6, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }
}
