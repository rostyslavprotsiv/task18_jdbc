package com.rostyslavprotsiv.model.DAO.implementation;

import com.rostyslavprotsiv.model.DAO.IDeliveryPointDAO;
import com.rostyslavprotsiv.model.connectionmanager.ConnectionManager;
import com.rostyslavprotsiv.model.entity.DeliveryPoint;
import com.rostyslavprotsiv.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class DeliveryPointDAOImpl implements IDeliveryPointDAO {
    private static final String FIND_ALL = "SELECT * FROM delivery_point";
    private static final String DELETE = "DELETE FROM delivery_point WHERE " +
            "id=?";
    private static final String CREATE = "INSERT delivery_point (id, address," +
            "city, point_id) VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE delivery_point SET address=?, " +
            "city=?, point_id=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM delivery_point" +
            " WHERE id=?";
    private static final String FIND_ADDRESS_BY_CITY = "SELECT address FROM " +
            "delivery_point WHERE city=?";

    @Override
    public List<DeliveryPoint> findAll() throws SQLException {
        List<DeliveryPoint> deliveryPoints = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    deliveryPoints
                            .add((DeliveryPoint) new Transformer(DeliveryPoint.class)
                                    .fromResultSetToEntity(resultSet));
                }
            }
        }
        return deliveryPoints;
    }

    @Override
    public DeliveryPoint findById(Integer id) throws SQLException {
        DeliveryPoint entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity =
                            (DeliveryPoint) new Transformer(DeliveryPoint.class)
                                    .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(DeliveryPoint entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getAddress());
            ps.setString(3, entity.getCity());
            ps.setString(4, entity.getPointId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(DeliveryPoint entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getAddress());
            ps.setString(2, entity.getCity());
            ps.setString(3, entity.getPointId());
            ps.setInt(4, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public List<String> getAddressByCity(String city) throws SQLException {
        List<String> addresses = new LinkedList<>();
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn
                .prepareStatement(FIND_ADDRESS_BY_CITY)) {
            ps.setString(1, city);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    addresses.add(resultSet.getString(resultSet.getString(1)));
                }
            }
            return addresses;
        }
    }
}
