package com.rostyslavprotsiv.model.DAO.implementation;

import com.rostyslavprotsiv.model.DAO.IClientDAO;
import com.rostyslavprotsiv.model.connectionmanager.ConnectionManager;
import com.rostyslavprotsiv.model.entity.Client;
import com.rostyslavprotsiv.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class ClientDAOImpl implements IClientDAO {
    private static final String FIND_ALL = "SELECT * FROM client";
    private static final String DELETE = "DELETE FROM client WHERE id=?";
    private static final String CREATE = "INSERT client (id, name, surname," +
            " middle_name, package_count, all_name) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE client SET name=?, " +
            "surname=?, middle_name=?,package_count=?, all_name=?  WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM client WHERE id=?";
    private static final String FIND_BY_SURNAME = "SELECT * FROM client" +
            " WHERE surname=?";

    @Override
    public List<Client> findAll() throws SQLException {
        List<Client> clients = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    clients.add((Client) new Transformer(Client.class).
                            fromResultSetToEntity(resultSet));
                }
            }
        }
        return clients;
    }

    @Override
    public Client findById(Integer id) throws SQLException {
        Client entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (Client) new Transformer(Client.class).
                            fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Client entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getName());
            ps.setString(3, entity.getSurname());
            ps.setString(4, entity.getMiddleName());
            ps.setInt(5, entity.getPackageCount());
            ps.setString(6, entity.getAllName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(Client entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getName());
            ps.setString(2, entity.getSurname());
            ps.setString(3, entity.getMiddleName());
            ps.setInt(4, entity.getPackageCount());
            ps.setString(5, entity.getAllName());
            ps.setInt(6, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public List<Client> getBySurname(String surname) throws SQLException {
        List<Client> clients = new LinkedList<>();
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(FIND_BY_SURNAME)) {
            ps.setString(1, surname);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    clients.add((Client) new Transformer(Client.class).
                            fromResultSetToEntity(resultSet));
                }
            }
            return clients;
        }
    }
}
