package com.rostyslavprotsiv.model.DAO.implementation;

import com.rostyslavprotsiv.model.DAO.IGettingPackageDAO;
import com.rostyslavprotsiv.model.connectionmanager.ConnectionManager;
import com.rostyslavprotsiv.model.entity.GettingPackage;
import com.rostyslavprotsiv.model.transformer.Transformer;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class GettingPackageDAOImpl implements IGettingPackageDAO {
    private static final String FIND_ALL = "SELECT * FROM getting_package";
    private static final String DELETE = "DELETE FROM getting_package WHERE " +
            "id=?";
    private static final String CREATE = "INSERT getting_package (id, got," +
            "price, sending_package, to_client_id," +
            " to_point_id) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE getting_package SET got=?, " +
            "price=?, sending_package_id=?, to_client_id=?, " +
            "to_point_id=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM getting_package" +
            " WHERE id=?";
    private static final String UPDATE_GOT_BY_ID = "UPDATE getting_package " +
            "SET got=?, WHERE id=?";


    @Override
    public List<GettingPackage> findAll() throws SQLException {
        List<GettingPackage> gettingPackages = new LinkedList<>();
        Connection connection = ConnectionManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    gettingPackages.add(
                            (GettingPackage) new Transformer(GettingPackage.class)
                                    .fromResultSetToEntity(resultSet));
                }
            }
        }
        return gettingPackages;
    }

    @Override
    public GettingPackage findById(Integer id) throws SQLException {
        GettingPackage entity = null;
        Connection connection = ConnectionManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity =
                            (GettingPackage) new Transformer(GettingPackage.class)
                                    .fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(GettingPackage entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setBoolean(2, entity.isGot());
            ps.setBigDecimal(3, entity.getPrice());
            ps.setInt(4, entity.getSendingPackageId());
            ps.setInt(5, entity.getToClientId());
            ps.setInt(6, entity.getToPointId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(GettingPackage entity) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE)) {
            ps.setBoolean(1, entity.isGot());
            ps.setBigDecimal(2, entity.getPrice());
            ps.setInt(3, entity.getSendingPackageId());
            ps.setInt(4, entity.getToClientId());
            ps.setInt(5, entity.getToPointId());
            ps.setInt(6, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }

    @Override
    public void updateIsGotById(boolean isGot, Integer id) throws SQLException {
        Connection conn = ConnectionManager.getConnection();
        try (PreparedStatement ps = conn.prepareStatement(UPDATE_GOT_BY_ID)) {
            ps.setBoolean(1, isGot);
            ps.setInt(2, id);
            ps.executeUpdate();
        }
    }
}
