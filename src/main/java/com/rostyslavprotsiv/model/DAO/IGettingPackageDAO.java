package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.GettingPackage;

import java.sql.SQLException;

public interface IGettingPackageDAO extends IGeneralDAO<GettingPackage, Integer> {
    void updateIsGotById(boolean isGot, Integer id) throws SQLException;
}
