package com.rostyslavprotsiv.model.DAO.annotation.enums;

public enum CascadeType {
    ALL, DETACH, MERGE, PERSIST, REFRESH, REMOVE
}
