package com.rostyslavprotsiv.model.DAO.annotation;

import com.rostyslavprotsiv.model.DAO.annotation.enums.CascadeType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OneToMany {
    CascadeType type();
}
