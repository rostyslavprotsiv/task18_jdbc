package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.RedirectedPackage;

import java.sql.SQLException;

public interface IRedirectedPackageDAO extends IGeneralDAO<RedirectedPackage, Integer> {
    public int createWithoutID(RedirectedPackage entity) throws SQLException;
}
