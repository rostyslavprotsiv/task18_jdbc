package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.Client;

import java.sql.SQLException;
import java.util.List;

public interface IClientDAO extends IGeneralDAO<Client, Integer> {
    List<Client> getBySurname(String surname) throws SQLException;
}
