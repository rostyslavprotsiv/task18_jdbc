package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.SendingPackage;

public interface ISendingPackageDAO extends IGeneralDAO<SendingPackage, Integer> {
}
