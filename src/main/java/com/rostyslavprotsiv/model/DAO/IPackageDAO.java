package com.rostyslavprotsiv.model.DAO;

import com.rostyslavprotsiv.model.entity.Package;

public interface IPackageDAO extends IGeneralDAO<Package, Integer> {
}
