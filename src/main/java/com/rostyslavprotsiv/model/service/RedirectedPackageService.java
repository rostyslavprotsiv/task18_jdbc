package com.rostyslavprotsiv.model.service;

import com.rostyslavprotsiv.model.DAO.implementation.RedirectedPackageDAOImpl;
import com.rostyslavprotsiv.model.entity.RedirectedPackage;

import java.sql.SQLException;
import java.util.List;

public class RedirectedPackageService {
    public List<RedirectedPackage> findAll() throws SQLException {
        return new RedirectedPackageDAOImpl().findAll();
    }

    public RedirectedPackage findById(Integer id) throws SQLException {
        return new RedirectedPackageDAOImpl().findById(id);
    }

    public int create(RedirectedPackage entity) throws SQLException {
        return new RedirectedPackageDAOImpl().create(entity);
    }

    public int update(RedirectedPackage entity) throws SQLException {
        return new RedirectedPackageDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new RedirectedPackageDAOImpl().delete(id);
    }

    public int createWithoutID(RedirectedPackage entity) throws SQLException {
        return new RedirectedPackageDAOImpl().createWithoutID(entity);
    }
}
