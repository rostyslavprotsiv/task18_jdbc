package com.rostyslavprotsiv.model.service;

import com.rostyslavprotsiv.model.DAO.implementation.ClientDAOImpl;
import com.rostyslavprotsiv.model.entity.Client;

import java.sql.SQLException;
import java.util.List;

public class ClientService {
    public List<Client> findAll() throws SQLException {
        return new ClientDAOImpl().findAll();
    }

    public Client findById(Integer id) throws SQLException {
        return new ClientDAOImpl().findById(id);
    }

    public int create(Client entity) throws SQLException {
        return new ClientDAOImpl().create(entity);
    }

    public int update(Client entity) throws SQLException {
        return new ClientDAOImpl().update(entity);
    }

    public int delete(Integer id) throws SQLException {
        return new ClientDAOImpl().delete(id);
    }

    public List<Client> getBySurname(String surname) throws SQLException {
        return new ClientDAOImpl().getBySurname(surname);
    }
}
