package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.service.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

public final class Controller {
    private final Logger LOGGER = LogManager.getLogger(Controller.class);
    private final ClientService CLIENT_SERVICE = new ClientService();
    private final DeliveryPointService DELIVERY_POINT_SERVICE
            = new DeliveryPointService();
    private final GettingPackageService GETTING_PACKAGE_SERVICE
            = new GettingPackageService();
    private final PackageService PACKAGE_SERVICE = new PackageService();
    private final RedirectedPackageService REDIRECTED_PACKAGE_SERVICE
            = new RedirectedPackageService();
    private final SendingPackageService SENDING_PACKAGE_SERVICE
            = new SendingPackageService();
    private final MetaDataService META_DATA_SERVICE
            = new MetaDataService();


    public String getAllTables() {
        StringBuilder all = new StringBuilder();
        try {
            all.append("client \n");
            CLIENT_SERVICE.findAll().forEach(s -> all.append(s).append("\n"));
            all.append("delivery_point \n");
            DELIVERY_POINT_SERVICE.findAll()
                    .forEach(s -> all.append(s).append("\n"));
            all.append("getting_package \n");
            GETTING_PACKAGE_SERVICE.findAll()
                    .forEach(s -> all.append(s).append("\n"));
            all.append("package \n");
            PACKAGE_SERVICE.findAll().forEach(s -> all.append(s).append("\n"));
            all.append("redirected_package \n");
            REDIRECTED_PACKAGE_SERVICE.findAll()
                    .forEach(s -> all.append(s).append("\n"));
            all.append("sending_package \n");
            SENDING_PACKAGE_SERVICE.findAll()
                    .forEach(s -> all.append(s).append("\n"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return all.toString();
    }

    public String getAllStructure() {
        StringBuilder allStructure = new StringBuilder();
        try {
            META_DATA_SERVICE.getTablesStructure().forEach(s-> allStructure
                    .append(s).append("\n"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allStructure.toString();
    }

    public boolean deleteAndMoveToRedirected(int deletedId) {
        try {
            return GETTING_PACKAGE_SERVICE.deleteAndMoveToRedirected(deletedId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    /*
                String logMessage = "Error with validating";
            LOGGER.info(logMessage);
            LOGGER.warn(logMessage);
            LOGGER.error(logMessage);
     */
}
